# Copyright (c) Leon Barrett 2023
# Licensed under the GPLv3, the same as the packaged Python app.
rec {
  description = "Utility to upload Magic the Gathering Arena data to 17Lands.com";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
  (let
    core = flake-utils.lib.eachDefaultSystem
    (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
    in rec {
      packages.seventeenlands = pkgs.python3Packages.buildPythonApplication {
        name = "seventeenlands";
        meta = {
          description = description;
          author = "Robert Conroy";
          homepage = "https://www.17lands.com/";
          license = pkgs.lib.licenses.gpl3;
        };
        src = pkgs.fetchFromGitHub {
          owner = "rconroy293";
          repo = "mtga-log-client";
          rev = "927310e02657dc87354b9eb32fa80279c955c040";
          sha256 = "sha256-tuzifipoohvpn1dNSYP84zF1HBtWB+CUu4bKMKe1IbM=";
        };
        sourceRoot = "source/src/python";
        propagatedBuildInputs = with pkgs.python3Packages ; [ python-dateutil requests tkinter ];
            # Somehow the tests require HOME to be writable.
            prePatch = "export HOME=.";
          };
          defaultPackage = packages.seventeenlands;
          apps.seventeenlands = flake-utils.lib.mkApp { drv = self.packages.${system}.seventeenlands; };
          defaultApp = apps.seventeenlands;
        });
  in core // {
    homeManagerModules = {
      seventeenlands = ({ config, pkgs, lib, ... }:
      let 
        cfg = config.programs.seventeenlands;
        system = pkgs.system;
        seventeenlands = core.packages.${system}.seventeenlands;
      in {
        options.programs.seventeenlands = {
          enable = lib.mkOption {
            type = lib.types.bool;
            default = false;
            description = "Whether to enable the 17lands reporting service";
          };
          token = lib.mkOption {
            type = lib.types.nullOr lib.types.str;
            default = null;
            description = "Your 17lands client token from https://www.17lands.com/account";
          };
        };
        config = lib.mkIf cfg.enable (lib.mkMerge [
          {
            home.packages = [ seventeenlands ];
            systemd.user.services.seventeenlands = {
              Unit = {
                Description = description;
              };
              Install = { WantedBy = [ "default.target" ]; };
              Service = {
                ExecStart = "${seventeenlands}/bin/seventeenlands";
              };
            };
          }
          (lib.mkIf (cfg.token != null) {
            home.file.".mtga_follower.ini".text = ''
              [client]
              token = ${cfg.token}
            '';
          })
        ]);
      });
    };
  });
}

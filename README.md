# 17 Lands Nix Flake

This is a [Nix flake](https://nixos.wiki/wiki/Flakes) for installing the [17
Lands](https://17lands.com/) [seventeenlands draft recorder Python
app](https://github.com/rconroy293/mtga-log-client/).

## Usage

1. Install [Nix](https://nixos.org/download.html).
2. Enable [Nix flakes support](https://nixos.wiki/wiki/Flakes) (if needed).
3. `nix run gitlab:leon-barrett/seventeenlands-flake`

## License
This flake is licensed under the GPLv3, the same as the packaged Python app.
